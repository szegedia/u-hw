define(() => {
  window.assert = chai.assert
  window.expect = chai.expect
  mocha.setup('bdd')

  require.config({
    baseUrl: '../build/js/',
    paths: {
      // app
      app: 'app/app',
      collection: 'app/collection',
      model: 'app/model',
      view: 'app/view',
      store: 'app/store',

      // model
      videoList: 'model/videoList',
      watchList: 'model/watchList',

      // view
      timerView: 'view/timerView',
      sortView: 'view/sortView',
      filterView: 'view/filterView',
      videoView: 'view/videoView',
      modalView: 'view/modalView',
      videoListView: 'view/videoListView',
      watchListView: 'view/watchListView',

      // utils
      emitter: 'utils/emitter'
    }
  });

  require([
    // app
    'specs/app/app.spec.js',
    'specs/app/collection.spec.js',
    'specs/app/model.spec.js',
    'specs/app/store.spec.js',
    'specs/app/view.spec.js',

    // utils
    'specs/utils/emitter.spec.js',

    // model
    'specs/model/videoList.spec.js',
    'specs/model/watchList.spec.js',

    // view
    'specs/view/filterView.spec.js',
    'specs/view/sortView.spec.js',
    'specs/view/timerView.spec.js',
    'specs/view/modalView.spec.js',
    'specs/view/videoListView.spec.js',
    'specs/view/videoView.spec.js',
    'specs/view/watchListView.spec.js',
  ], () => {
    mocha.run()
  })
})
