'use strict';

define(['collection', 'model'], (Collection, Model) => {
	describe('Collection', function () {
    let list;
    const data = { id: 0, title: 'test title' };
    const modelList = [{
      id: 0,
      type: 'recorded',
      isLive: false
    }, {
      type: 'channel',
      isLive: true
    }, {
      type: 'channel',
      isLive: false
    }];

    beforeEach(() => {
			list = new Collection();
		});

		it('instance', () => {
	    expect(list instanceof Collection).to.be.true;
	  });

    it('add() should add new item', () => {
			const model = new Model(data);
      expect(list.add(data).data).to.equal(model.data);
    });

    it('get() should give back an item', () => {
			list.add(data);
      expect(list.get(0).data.id).to.equal(data.id);
			list.add({id: 1, title: 'test title #2'});
			expect(list.get()).to.have.lengthOf(2);
    });

    it('set() should update items', () => {
      const model = list.add(data);
			const updateData = {id: 0, title: 'new test title'};
      list.set([updateData]);
      expect(list.get(0).data.title).to.equal(updateData.title);
    });

    it('set() should add new items', () => {
      const model = list.add(data);
      const newData = {id: 1, title: 'test title #2'};
      list.set([data, newData]);

      expect(list.get()).to.have.lengthOf(2);
    });

    it('update() should update items', () => {
			const model = list.add(data);
			const updateData = {id: 0, title: 'new test title'};
      list.update(updateData);

      expect(list.get(0).data.title).to.equal(updateData.title);
    });

    it('remove() should remove item', () => {
      const model = list.add(data);
      list.remove(model);

      expect(list.get()).to.have.lengthOf(0);
    });
	});
});
