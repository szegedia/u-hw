'use strict';

define(['model', 'emitter'], (Model, Emitter) => {
	describe('Model', function () {
    let model
    const data = { id: 0, title: 'test title' };

    beforeEach(() => {
			model = new Model(data);
		});

		it('instance', () => {
	    expect(model instanceof Model).to.be.true;
	    expect(model instanceof Emitter).to.be.true;
	  });

    it('set() should update model data', () => {
      model.set({ title: 'new test title' });
	    expect(model.data.title).to.equal('new test title');
	  });

    it('destroy() should trigger an event', () => {
      const handler = sinon.spy();
      model.on('model::destroy', handler);
      model.destroy();
	    expect(handler.called).to.be.true;
	  });
	});
});
