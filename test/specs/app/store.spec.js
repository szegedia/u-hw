'use strict';

define(['store', 'emitter', 'model'], (Store, Emitter, Model) => {
	describe('Store', function () {
    let store;
    const model = new Model({id: 0, title: 'test title'});

    beforeEach(() => {
      localStorage.setItem('test', '');
			store = new Store({key: 'test'});
		});

		it('instance', () => {
	    expect(store instanceof Store).to.be.true;
	    expect(store instanceof Emitter).to.be.true;
	  });

    it('add() should add data into store', () => {
      store.add(model);
	    expect(store.get(model.data.id).id).to.equal(model.data.id);
	  });

    it('get() should give back the saved data', () => {
			const newModel = new Model({id: 1, title: 'test title #2'});
	    store.add(model);
	    expect(store.get(model.data.id).id).to.deep.equal(model.data.id);
			store.add(newModel);
			expect(store.get()).to.have.lengthOf(2);
	  });

    it('update() should update an item', () => {
      const data = {id: 0, title: 'new test title'};
      store.add(model);
      store.update(data);
	    expect(store.get(model.data.id).title).to.equal(data.title);
	  });

    it('remove() should remove an item', () => {
      store.add(model);
      store.remove(model);
	    expect(store.get()).to.have.lengthOf(0);
	  });

	});
});
