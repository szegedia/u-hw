'use strict';

define(['view'], (View) => {
	describe('View', function () {
    let view;
    const $app = $('#app');
    View.prototype.init = () => {};

    beforeEach(() => {
			view = new View({ el: $app });
		});

		it('instance', () => {
	    expect(view instanceof View).to.be.true;
	  });
	});
});
