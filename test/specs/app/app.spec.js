'use strict';

define(['app', 'model'], (App, Model) => {
	describe('App', function () {
    const $el = $('#app');
		const spyStart = sinon.stub(App.prototype, 'start', sinon.spy());
		let app, model;


		beforeEach(() => {
			app = new App({ el: $el });
			model = new Model({ id: 0 });
			// clear localStorage
			localStorage.setItem('watch', '');
		});

		it('instance', () => {
	    expect(app instanceof App).to.be.true;
	  });

		it('starts() the fetch loop', () => {

			expect(spyStart.called).to.be.true;
	  });

		it('updateTimer() should update the timer interval', () => {
			app.updateTimer(1234);
			expect(app.interval).to.equal(1234);
	  });

		it('addStore() should add to the store', () => {
			app.addStore(model);

			expect(localStorage.getItem('watch')).to.equal(JSON.stringify([model.data]));
	  });

		it('removeStore() should remove from the store', () => {
			app.list.add(model.data);
			app.addStore(model);
			app.removeStore(model);

			expect(localStorage.getItem('watch')).to.equal(JSON.stringify([]));
	  });

	});
});
