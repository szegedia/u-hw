'use strict';

define(['videoList', 'store', 'emitter', 'collection'], (videoList, Store, Emitter, Collection) => {
	describe('videoList', function () {
		let list;
		const options = {
			store: new Store({key: 'test'}),
			emitter: new Emitter(),
			collection: new Collection()
		};

		beforeEach(() => {
			list = new videoList(options);
		});


		it('instance', () => {
	    expect(list instanceof videoList).to.be.true;
	    expect(list instanceof Collection).to.be.true;
	  });

		it('fetch() should get data via ajax', () => {
			return list.fetch().then(data => {
				expect(data).to.not.be.null;
			})
	  });

		it('sortList() ', () => {
			const testList = [{data: {id: 2}}, {data: {id: 1}}, {data: {id: 0}}];
			list.setFilter('id');

			expect(list.sortList(testList)).to.deep.equal(testList.reverse());
	  });

		it('filterList() ', () => {
			const testList = [{data: {id: 2, type: 'recorded'}}, {data: {id: 1, type: 'recorded'}}, {data: {id: 0}}];
			list.setFilter('video');

			expect(list.filterList(testList)).to.deep.equal([{data: {id: 2, type: 'recorded'}}, {data: {id: 1, type: 'recorded'}}]);
	  });
	});
});
