'use strict';

define(['watchList', 'store', 'collection'], (watchList, Store, Collection) => {
	describe('watchList', function () {
		const model = [{ id: 0 }];
		const options = {
			store: new Store({key: 'test'})
		};

		beforeEach(() => {
			// clear localStorage
			localStorage.setItem('test', JSON.stringify(model));
		});

		it('instance', () => {
	    const list = new watchList(options);
	    expect(list instanceof watchList).to.be.true;
	    expect(list instanceof Collection).to.be.true;
	  });

		it('fetch() should get data from localStorage', () => {
	    const list = new watchList(options);
			const saved = list.fetch();
			expect(saved).to.deep.equal(model);
	  });
	});
});
