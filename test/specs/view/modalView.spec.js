'use strict';

define(['modalView', 'emitter'], (modalView, Emitter) => {
	describe('modalView', function () {
    const $el = $('.modal');
    const $backdrop = $('.modal-backdrop');
		const data = { id: 0, title: 'test title', description: 'test description' };
    const emitter = new Emitter();
    let modal;

		beforeEach(() => {
			 modal = new modalView({ el: $el, backdrop: $backdrop, emitter });
		});

		it('instance', () => {
	    expect(modal instanceof modalView).to.be.true;
	  });

    it('show() should display the modal', () => {
      modal.show();
	    expect($el.hasClass('hidden')).to.be.false;
	  });

    it('hide() should hide the modal', () => {
      modal.show();
      modal.hide();
	    expect($el.hasClass('hidden')).to.be.true;
	  });

    it('update() should update modal content', () => {
      modal.update(data);
	    expect($el.find('.modal__title').text()).to.equal(data.title);
	    expect($el.find('.description' ,'.modal__content').text()).to.equal(data.description);
	  });
	});
});
