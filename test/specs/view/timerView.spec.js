'use strict';

define(['timerView', 'emitter'], (timerView, Emitter) => {
	describe('timerView', function () {
    const $el = $('.timer');
    const emitter = new Emitter();
    let timer;

		beforeEach(() => {
			 timer = new timerView({ el: $el, emitter });
		});

		it('instance', () => {
	    expect(timer instanceof timerView).to.be.true;
	  });

    it('onChange should trigger timer::change event', () => {
      const cb = sinon.spy();
      emitter.on('timer::change', cb);
      $el.val(3000).change();
	    expect(cb.called).to.be.true;
	  });

	});
});
