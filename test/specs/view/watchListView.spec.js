'use strict';

define(['watchListView', 'collection', 'model', 'emitter'], (watchListView, Collection, Model, Emitter) => {
	describe('watchListView', function () {
    const $el = $('.watch-list');
		const data = { id: 0, title: 'test title' };
    const emitter = new Emitter();
    let list, collection;

		beforeEach(() => {
      collection = new Collection();
      list = new watchListView({ el: $el, collection, emitter: emitter });
		});

		it('instance', () => {
	    expect(list instanceof watchListView).to.be.true;
	  });

    it('append() should add element to list', () => {
      const model = new Model(data);
      list.append(model);
	    expect($el.find('.video')).to.have.lengthOf(1);
	  });
	});
});
