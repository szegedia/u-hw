'use strict';

define(['videoListView', 'videoList', 'collection', 'emitter', 'model', 'store'], (videoListView, videoList, Collection, Emitter, Model, Store) => {
	describe('videoListView', function () {
    const $el = $('.video-list');
		const data = { id: 0, title: 'test title' };
    const emitter = new Emitter();
    const store = new Store({ key: 'test' });
    const collection = new videoList({ store, emitter });
    const model = new Model(data);
    let listView;

		beforeEach(() => {
			listView = new videoListView({ el: $el, emitter, collection });
			localStorage.setItem('test', '');
		});

		it('instance', () => {
	    expect(listView instanceof videoListView).to.be.true;
	  });

		it('collection add() should add new element', () => {
			const fetchSpy = sinon.stub(videoList.prototype, 'fetch', sinon.spy());
			const testCollection = new videoList({ store, emitter });
			const testListView = new videoListView({ el: $el, emitter, collection: testCollection });

			testCollection.add(model.data);
			expect($el.find('.title', '.video').text()).to.equal(data.title);
			fetchSpy.restore();
		});

		it('render() should render elements', () => {
			listView.collection.set([model.data]);
			expect($el.find('.title', '.video').text()).to.equal(data.title);
		});
	});
});
