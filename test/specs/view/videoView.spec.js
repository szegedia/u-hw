'use strict';

define(['videoView', 'model', 'emitter'], (videoView, Model, Emitter) => {
	describe('videoView', function () {
    const $el = $('.video-list');
		const data = { id: 0, title: 'test title' };
    const model = new Model(data);
    const emitter = new Emitter();
    let video;

		beforeEach(() => {
			 video = new videoView({ model, emitter: this.emitter });
		});

		it('instance', () => {
	    expect(video instanceof videoView).to.be.true;
	  });

    it('model change should update view elemnts', () => {
      model.set({title: 'new test title'});
	    expect(video.$el.find('.title').text()).to.equal('new test title');
	  });

    it('model destroy should remove view', () => {
      model.destroy();
	    expect(video.$el).to.be.undefined;
	  });
	});
});
