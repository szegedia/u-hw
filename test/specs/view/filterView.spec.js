'use strict';

define(['filterView'], (filterView) => {
	describe('filterView', function () {
    const $el = $('.filters');
    const $btns = $el.find('.filter-btn');
    let filter;

		beforeEach(() => {
			 filter = new filterView({ el: $el });
		});

		it('instance', () => {
	    expect(filter instanceof filterView).to.be.true;
	  });

    it('click should call filter method', () => {
      const filterMethod = sinon.stub(filterView.prototype, 'filterList', sinon.spy());
      filter = new filterView({ el: $el });

      $btns.eq(0).trigger('click');
	    expect(filterMethod.called).to.be.true;
	  });

    it('render() should give back $el', () => {
	    expect(filter.render()).to.equal($el);
	  });
	});
});
