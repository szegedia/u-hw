'use strict';

define(['sortView', 'emitter', 'collection'], (sortView, Emitter, Collection) => {
	describe('sortView', function () {
    const $el = $('.sort');
    const $sortBy = $el.find('.sort-by');
    const $order = $el.find('.order');
		const $orderIcon = $order.find('.order-icon');
    let sort, collection, emitter;

		beforeEach(() => {
       collection = new Collection();
			 emitter = new Emitter();
			 sort = new sortView({ el: $el, collection, emitter });
		});

		it('instance', () => {
	    expect(sort instanceof sortView).to.be.true;
	  });

    it('.sort-by change should call setStort()', () => {
      const setSort = sinon.stub(sortView.prototype, 'setSort', sinon.spy());
      sort = new sortView({ el: $el, collection, emitter });

      $sortBy.val('title').change();
	    expect(setSort.called).to.be.true;
      setSort.restore();
	  });

    it('.order click should call toggleOrder()', () => {
      const toggleOrder = sinon.stub(sortView.prototype, 'toggleOrder', sinon.spy());
      sort = new sortView({ el: $el, collection, emitter });

      $order.trigger('click');
	    expect(toggleOrder.called).to.be.true;
      toggleOrder.restore();
	  });

    it('sort() should call sort method on collection', () => {
			const sortList = sinon.stub(sortView.prototype, 'sortList', sinon.spy());
      sort = new sortView({ el: $el, collection, emitter });

      sort.sortList();
	    expect(sortList.called).to.be.true;
			sortList.restore();
	  });

    it('setSort() should update sort parameters and call sort()', () => {
      sort.setSort({by: 'country', level: 'location'});
	    expect(sort.by).to.equal('country');
	    expect(sort.level).to.equal('location');
	  });

    it('order() should toggle orderType paramter', () => {
      const original = sort.order;
      const update = sinon.stub(sortView.prototype, 'update', sinon.spy());

      sort.toggleOrder();
	    expect(sort.order).not.to.equal(original);
      update.restore();
	  });

    it('update() should update .order-icon', () => {
			const original = $orderIcon.hasClass('fa-sort-amount-asc');
      sort.update();
	    expect($orderIcon.hasClass('fa-sort-amount-asc')).not.to.equal(original);
	  });

	});
});
