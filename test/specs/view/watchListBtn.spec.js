'use strict';

define(['watchListBtn', 'model', 'emitter'], (watchListBtn, Model, Emitter) => {
	describe('watchListBtn', function () {
    const $el = $('<div></div>');
		const data = { id: 0, title: 'test title' };
    const emitter = new Emitter();
    let listBtn, model;

		beforeEach(() => {
      model = new Model(data);
			listBtn = new watchListBtn({ el: $el, emitter, model });
		});

		it('instance', () => {
	    expect(listBtn instanceof watchListBtn).to.be.true;
	  });

    it('.add-watch-btn click should trigger watchlist::add', () => {
      const cb = sinon.spy();
      emitter.on('watchlist::add', cb);

      $el.find('.add-watch-btn').trigger('click');
	    expect(cb.called).to.be.true;
	  });

    it('.remove-watch-btn click should trigger watchlist::add', () => {
      const cb = sinon.spy();
      emitter.on('watchlist::remove', cb);
      model.set({saved: true});
      $el.find('.remove-watch-btn').trigger('click');
	    expect(cb.called).to.be.true;
	  });


    it('model change update saved property', () => {
      // saved
      model.set({saved: true})
	    expect(listBtn.saved).to.be.true;

      // unsaved
      model.set({saved: false})
      expect(listBtn.saved).to.be.false;
	  });
	});
});
