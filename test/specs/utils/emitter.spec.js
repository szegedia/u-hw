'use strict';

define(['emitter'], (Emitter) => {
	describe('Emitter', function () {
		let emitter;

		beforeEach(() => {
			emitter = new Emitter();
		});

		it('instance', () => {
	    expect(emitter instanceof Emitter).to.be.true;
	  });

    it('trigger() should trigger call a listener', () => {
      const testCb = sinon.spy();
	    emitter.on('test', testCb);
      emitter.trigger('test');

      expect(testCb.called).to.be.true;
	  });

    it('on() should add a listener cb', () => {
      const testCb = sinon.spy();
	    emitter.on('test', testCb);

      expect(emitter.listeners['test'][0]).to.equal(testCb);
	  });

    it('off() should remove listener cb', () => {
      const testCb = sinon.spy();
      const testCb2 = sinon.spy();
	    emitter.on('test', testCb);
	    emitter.on('test', testCb2);
	    emitter.off('test', testCb);

      expect(emitter.listeners['test']).to.have.lengthOf(1);
	  });
	});
});
