# Ustream homework

run `npm install` to install node packages

use `npm run start` to watch and build _src_ and _test_ folders

### tests
_test folder will be the destination folder

run `npm run test` to build

### docs

run `npm run generate-docs` to generate documentation