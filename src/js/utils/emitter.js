'use strict';
define('emitter', [], () => {

  /**
   * Emitter class
   * @class Emitter
   * @classdesc Simple emitter with pub-sub implementation
   */
  class Emitter {
    constructor() {
      this.listeners = [];
    }

    /**
     * trigger - trigger an event
     *
     * @param  {string} e    event name
     * @param  {object} data data send via event
     * @return {this}
     */
    trigger(e, data) {
      if(!this.listeners[e] || !this.listeners[e].length) {
        return false;
      }

      this.listeners[e].forEach(listener => {
        listener(e, data);
      });

      return this;
    }

    /**
     * on - add listener
     *
     * @param  {string} e    event name
     * @param  {function} cb callback function
     * @return {this}
     */
    on(e, cb) {
      this.listeners[e] || (this.listeners[e] = []);
      this.listeners[e].push(cb);

      return this;
    }

    /**
     * off - remove listener
     *
     * @param  {string} e  event name
     * @param  {function} cb callback function
     * @return {this}
     */
    off(e, cb) {
      this.listeners[e] = this.listeners[e].filter(listener => listener !== cb);

      return this;
    }
  }

  return Emitter;
});
