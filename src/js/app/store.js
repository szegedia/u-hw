'use strict'

define('store', ['emitter'], (Emitter) => {

  /**
   * @class Store
   * @classdesc simple localStorage interface
   * @extends Emitter
   */
  class Store extends Emitter {

    /**
     * constructor - store contructor
     *
     * @param  {object} { key } localStorage item key
     * @return {Store}
     */
    constructor({ key }) {
      super();
      this.key = key;
    }


    /**
     * get - get item(s)
     *
     * @param  {number=} id
     * @return {array|object}
     */
    get(id) {
      const data = localStorage.getItem(this.key) || '[]';
      const parsed = JSON.parse(data);

      if(!!arguments.length) {
        return parsed.find(data => data.id === id);
      }

      return parsed;
    }


    /**
     * add - add new item to store and trigger 'saved' event on model
     *
     * @param  {Model} model
     * @return {this}
     */
    add(model) {
      const list = this.get();

      model.trigger('model::saved');
      localStorage.setItem(this.key, JSON.stringify([...list, model.data]));
      this.trigger('store::add', model.data);
      return this;
    }


    /**
     * update - update item in store
     *
     * @param  {data} data
     * @return {this}
     */
    update(data) {
      const list = this.get();
      const updatedList = list.map(saved => {
        if (saved.id === data.id) {
          return Object.assign({}, saved,  data);
        }
        return saved;
      });

      localStorage.setItem(this.key, JSON.stringify(updatedList));
      this.trigger('store::update', data);
      return this;
    }


    /**
     * remove - remove item from store
     *
     * @param  {number} id id of data
     * @return {this}
     */
    remove(model) {
      const list = this.get();
      const newList = list.filter(item => item.id !== model.data.id);

      model.trigger('model::unsaved');
      localStorage.setItem(this.key, JSON.stringify(newList));
      this.trigger('store::remove', model.data);
      return this;
    }
  }

  return Store;
});
