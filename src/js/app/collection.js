'use strict';

define('collection', ['emitter', 'model'], (Emitter, Model) => {

  /**
   * @class Collection
   * @extends Emitter
   */
  class Collection extends Emitter {
    constructor(options) {
      super(options);
      this.models = new Map();
    }

    /**
     * add - add new item in collection
     *
     * @param  {object} item new item data
     * @return {collection}      collection object
     */
    add(data) {
      const model = new Model(data);
      this.models.set(data.id, model);

      this.trigger('collection::add', model);
      return model;
    }


    /**
     * get - get element(s) from collection
     *
     * @param  {number=} id if id exists this will look up for model if not it gives you back the whole model list
     * @return {Model|Model[]}
     */
    get(id) {
      if(!!arguments.length) {
        return this.models.get(id);
      }

      return [...this.models].map(model => model[1]);
    }


    /**
     * set - set collection items
     * if data exists this will update in the collection
     * if data doesn't exists this will append to collection
     * data not in the items will by removed in the collection
     *
     * @param  {array} dataList data list
     * @return {this}
     */
    set(dataList = []) {
      const dataIds = dataList.map(data => data.id);
      const addItems = dataList.filter(data => !this.models.get(data.id));
      const updateItems = dataList.filter(data => this.models.get(data.id));
      const removeItems = [...this.models].filter(model => dataIds.indexOf(model[1].data.id) === -1).map(model => model[1]);

      // add new items
      if(!!addItems.length) {
        addItems.forEach(data => { this.add(data); });
        this.trigger('collection::listadd');
      }

      // update items
      if(!!updateItems.length) {
        updateItems.forEach(data => { this.update(data); });
        this.trigger('collection::listupdate');
      }

      // remove items
      if(!!removeItems.length) {
        removeItems.forEach(model => { this.remove(model); });
        this.trigger('collection::listremove');
      }

      return this;
    }


    /**
     * update - update model data in collection
     *
     * @param  {object} data
     * @return {this}
     */
    update(data) {
      const model = this.models.get(data.id);
      model.set(data);
      this.models.set(data.id, model);
      this.trigger('collection::update', model);

      return this;
    }


    /**
     * remove - remove model from collection
     *
     * @param  {Model} model
     * @return {this}
     */
    remove(model) {
      model.destroy();
      this.models.delete(model.data.id);
      this.trigger('collection::remove', model);

      return this;
    }
  }

  return Collection;
});
