'use strict'

define('model', ['emitter'], (Emitter) => {
  /**
   * @class Model
   * @extends Emitter
   */
  class Model extends Emitter {
    constructor(data) {
      super();
      this.data = data;

      this.on('model::saved', () => {
        this.set({ saved: true });
      });

      this.on('model::unsaved', () => {
        this.set({ saved: false });
      });
    }


    /**
     * set - update model data
     *
     * @param  {object} data
     * @return {Model}
     */
    set(data) {
      this.data = Object.assign({}, this.data, data);
      this.trigger('model::change', this);
      return this;
    }


    /**
     * destroy - trigger destroy event
     *
     * @return {Model}
     */
    destroy() {
      this.trigger('model::destroy', this);
      return this;
    }
  }

  return Model;
});
