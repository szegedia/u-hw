'use strict';

define('view', [], () => {

  /**
   * @class View
   * @classdesc simple view implementation
   */
  class View {

    /**
     * constructor - view constructor
     *
     * @param  {object} options
     * @param  {HTMLElement} options.el element to attach
     * @param  {Model=} options.model view's model
     * @param  {Collection=} options.collection view's collection
     * @param  {Emitter} options.emitter global emitter
     * @return {View}
     */
    constructor({ el, model, collection, emitter }) {
      this.$el = el;
      this.emitter = emitter;
      this.collection = collection;
      this.model = model;

      this.init();
    }
  }

  return View;
});
