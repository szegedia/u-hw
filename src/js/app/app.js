'use strict';

define('app', [
  'emitter',
  'store',
  'view',
  'collection',
  'timerView',
  'sortView',
  'filterView',
  'videoList',
  'modalView',
  'watchList',
  'videoListView',
  'watchListView'
], (Emitter, Store, View, Collection, timerView, sortView, filterView, videoList, modalView, watchList, videoListView, watchListView) => {

  /**
   * @class App
   * @extends View
   */
  class App extends View {
    init() {
      // config
      this.interval = 10000;

      // globals
      this.emitter = new Emitter();
      this.store = new Store({ key: 'watch' });

      // lists
      this.list = new videoList({ emitter: this.emitter, store: this.store });
      this.watchList = new watchList({ emitter: this.emitter, store: this.store });

      // event delegate
      this.emitter.on('timer::change', (e, val) => { this.updateTimer(val); });
      this.emitter.on('watchlist::add', (e, model) => { this.addStore(model); });
      this.emitter.on('watchlist::remove', (e, model) => { this.removeStore(model); });

      this.list.on('collection::listupdate', e => { this.updateStore(); });

      // start request loop
      this.start();

      // render items
      this.render();
    }


    /**
     * start - start fetch loop
     */
    start() {
      this.timer = setInterval(() => {
        this.list.fetch();
      }, this.interval);
    }

    /**
     * updateTimer - update fetch interval
     *
     * @param  {number} val interval
     */
    updateTimer(val) {
      clearTimeout(this.timer);
      this.interval = val;
      this.start();
    }


    /**
     * addStore - add model to store
     *
     * @param  {Model} model
     * @return {boolean=}       return false if the model already in the store
     */
    addStore(model) {
      if(!!this.store.get(model.data.id)) {
        return false;
      }
      this.store.add(model);
    }


    /**
     * removeStore - remove model from store
     *
     * @param  {Model} model
     */
    removeStore(model) {
      this.store.remove(model);
    }


    /**
     * updateStore - update items in store
     */
    updateStore() {
      const list = this.list.get();
      list.forEach(model => {
        this.store.update(model.data);
      });
    }


    /**
     * render - render the app view
     */     
    render() {
      const timer = new timerView({ el: this.$el.find('.timer'), emitter: this.emitter });
      const sort = new sortView({ el: this.$el.find('.sort'), emitter: this.emitter, collection: this.list });
      const filter = new filterView({ el: this.$el.find('.filters'), emitter: this.emitter, collection: this.list });
      const modal = new modalView({ el: $('.modal'), backdrop: $('.modal-backdrop'), emitter: this.emitter });

      const watchList = new watchListView({ el: this.$el.find('.watch-list'), emitter: this.emitter, collection: this.watchList });
      watchList.render();

      const listView = new videoListView({ el: this.$el.find('.video-list'), emitter: this.emitter, collection: this.list });
      listView.render();
    }
  }

  return App;
});
