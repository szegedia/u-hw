'use strict'

define('sortView', ['view'], (View) => {

  /**
   * @class sortView
   * @extends View
   */
  class sortView extends View {

    /**
     * init - initialize
     */
    init() {
      this.$orderIcon = this.$el.find('.order-icon');
      this.order = 'asc';
      this.level = null;
      this.by = 'id';

      this.$el.on('click', '.order', (e) => {
        this.toggleOrder();
        this.sortList();
        this.update();
      });

      this.$el.on('change', '.sort-by', (e) => {
        const $target = $(':selected', e.target);
        const options = {
          by: $target.val(),
          level: $target.data('level')
        };

        this.setSort(options);
        this.sortList();
      });
    }


    /**
     * sortList - trigger 'sort::change' event
     */
    sortList() {
      this.emitter.trigger('sort::change', [this.by, this.level, this.order]);
    }


    /**
     * setSort - update sort options
     *
     * @param  {options}
     * @param  {string} options.by
     * @param  {string} options.level
     * @return {this}
     */
    setSort({by, level}) {
      this.by = by;
      this.level = level;

      return this;
    }


    /**
     * toggleOrder - toggle order option
     *
     * @return {this}
     */
    toggleOrder() {
      this.order = this.order === 'asc' ? 'desc' : 'asc';
      return this
    }


    /**
     * update - update sort button
     *
     * @return {this}
     */
    update() {
      this.$orderIcon.toggleClass('fa-sort-amount-asc fa-sort-amount-desc');
      return this;
    }


    /**
     * render - gives back the $el
     *
     * @return {HTMLElement}
     */
    render() {
      return this.$el;
    }
  }

  return sortView;
});
