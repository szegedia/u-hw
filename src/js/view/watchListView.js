'use strict';

define('watchListView', ['view', 'videoView'], (View, videoView) => {

  /**
   * @class watchListView
   * @extends View
   */
  class watchListView extends View {

    /**
     * init - initialize
     */
    init() {
      this.collection.on('collection::add', (e, model) => { this.append(model); });
    }


    /**
     * append - append element to the list
     *
     * @param  {Model} model new item model
     */
    append(model) {
      const view = new videoView({ model, emitter: this.emitter });
      this.$el.append(view.render());
    }


    /**
     * render - render collection list
     *
     * @return {HTMLElement}  $el
     */
    render() {
      const list = this.collection.get();
      if(!list.length) {
        return false;
      }

      const views = list.map(model => {
        const view = new videoView({ model, emitter: this.emitter });
        return view.render();
      });

      this.$el.append(views);
      return this.$el;
    }
  }

  return watchListView;
});
