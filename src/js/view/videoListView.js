'use strict';

define('videoListView', ['view', 'videoView'], (View, videoView) => {

  /**
   * @class videoListView
   * @extends View
   */
  class videoListView extends View {

    /**
     * init - initialize
     */
    init() {
      this.collection.on('collection::add', (e, model) => { this.render(); });
      this.collection.on('collection::filterChanged', e => { this.render(); });
      this.collection.on('collection::sortChanged', e => { this.render(); });
      this.collection.on('collection::listupdate', e => { this.render(); });
    }


    /**
     * render - render list
     *
     * @return {HTMLElement} $el
     */
    render() {
      const list = this.collection.getList();

      if(!list.length) {
        return false;
      }

      const views = list.map(model => {
        const view = new videoView({ model, emitter: this.emitter });
        return view.render();
      });

      this.$el.html(views);
      return this.$el;
    }
  }

  return videoListView;
});
