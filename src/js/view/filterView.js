'use strict'

define('filterView', ['view'], (View) => {

  /**
   * @class filterView
   * @extends View
   */
  class filterView extends View {

    /**
     * init - initialize
     */
    init() {
      this.$el.on('click', '.filter-btn', (e) => {
        this.$el.find('.btn-primary').removeClass('btn-primary');
        $(e.target).addClass('btn-primary');
        this.filterList(e);
      });
    }


    /**
     * filterList - trigger 'filter::change' event
     *
     * @param  {Event} e
     */
    filterList(e) {
      const type = $(e.target).data('filter');
      this.emitter.trigger('filter::change', type);
    }


    /**
     * render - render the view
     *
     * @return {HTMLElement}  filter view
     */
    render() {
      return this.$el;
    }
  }

  return filterView;
});
