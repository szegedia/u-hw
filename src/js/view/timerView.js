'use strict'

define('timerView', ['view'], (View) => {

  /**
   * @class timerView
   * @extends View
   */
  class timerView extends View {

    /**
     * init - initialize
     */     
    init() {
      this.$el.on('change', (e) => {
        const val = $(e.target).val();
        this.emitter.trigger('timer::change', val);
      });
    }
  }

  return timerView;
});
