'use strict';

define('videoView', ['emitter', 'view'], (Emitter, View) => {

  /**
   * @class videoView
   * @extends View
   */
  class videoView extends View {

    /**
     * init - initialize
     */
    init() {
      this.$el = $('<div />', { 'class': 'video' });

      this.model.on('model::change', this.render.bind(this));
      this.model.on('model::destroy', this.destroy.bind(this));

      this.$el.on('click', '.video__info-btn', () => {
        this.emitter.trigger('modal::show', this.model.data);
        return false;
      });

      this.$el.on('click', '.watch-btn', () => {
        if(this.model.data.saved) {
          this.removeWatchList();
          return false;
        }

        this.addWatchList();
        return false;
      });
    }


    /**
     * addWatchList - trigger 'watchlist::add' event
     */
    addWatchList() {
      this.emitter.trigger('watchlist::add', this.model);
    }


    /**
     * removeWatchList - trigger 'watchlist::remove' event
     */
    removeWatchList() {
      this.emitter.trigger('watchlist::remove', this.model);
    }


    /**
     * destroy - remove $el element
     */
    destroy() {
      this.$el.remove();
      delete this.$el;
    }


    /**
     * render - render $el
     *
     * @return {HTMLElement} $el
     */
    render() {
      return this.$el.html(`
        <div class="video__image">
          <img src="${this.model.data.picture || ''}">
          <div class="channel-data">
            <strong class="live label ${this.model.data.isLive ? 'label-primary' : this.model.data.type === 'channel' ? '' : 'hidden'}">
              <i class="fa fa-video-camera"></i>
              <span class="">${this.model.data.isLive ? 'Live' : 'Offline'}</span>
            </strong>
            <strong class="recorded label ${this.model.data.type === 'recorded' ? '' : 'hidden'}">
              <i class="fa fa-film"></i> Video
            </strong>
          </div>
        </div>
        <div class="video__body">
          <div class="text-overflow">
            <strong class="title" title="${this.model.data.title || ''}">${this.model.data.title || ''}</strong>
          </div>
          <div class="video__desc desc">
            ${this.model.data.description || ''}
          </div>
          <strong class="viewers pull-right">${this.model.data.viewers || ''} views</strong>
          <button type="button" class="video__info-btn btn btn-default">Info</button>
          <span class="video__btns watch-btn">
            <button type="button" class="watch-btn btn">
              <i class="fa ${this.model.data.saved ? 'fa-check' : 'fa-clock-o'}" aria-hidden="true"></i>
              ${this.model.data.saved ? 'Remove' : 'Watch later'}
            </button>
          </span>
        </div>
      `);
    }
  }

  return videoView;
});
