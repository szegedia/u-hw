'use strict'

define('modalView', ['view'], (View) => {

  /**
   * @class modalView
   * @extends View
   */
  class modalView extends View {

    /**
     * constructor
     *
     * @param  {object} options
     * @param  {HTMLElement} options.backdrop backdrop element
     * @param  {Emitter} options.emitter global emitter
     * @return {modalView}
     */
    constructor(options) {
      super(options);
      this.$backdrop = options.backdrop;
      this.emitter = options.emitter;
    }


    /**
     * init - initialize
     */
    init() {
      this.$el.on('click', '[data-dismiss="modal"]', () => { this.hide(); });
      this.emitter.on('modal::show', (e, data) => {
        if(data) {
          this.update(data);
        }

        this.show(data);
      });
    }


    /**
     * show - show element and backdrop
     */
    show() {
      this.$el.removeClass('hidden');
      this.$backdrop.removeClass('hidden');
    }


    /**
     * hide - hide element and backdrop
     */
    hide() {
      this.$el.addClass('hidden');
      this.$backdrop.addClass('hidden');
    }


    /**
     * update - update the view
     *
     * @param  {array} data update data
     */
    update(data) {
      this.$el.find('.modal__title').text(data.title);
      function loop(data) {
        Object.keys(data).forEach(key => {
          if(typeof data[key] === 'object' && !Array.isArray(data[key])) {
            return loop.call(this, data[key]);
          }
          this.$el.find(`.${key}`, '.modal__content').text(data[key]);
        });
      }
      loop.call(this, data);
    }
  }

  return modalView;
});
