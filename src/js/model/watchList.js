'use strict';

define('watchList', ['store', 'collection', 'model'], (Store, Collection, Model) => {

  /**
   * @class watchList
   * @extends Collection
   */
  class watchList extends Collection {

    /**
     * constructor
     *
     * @param  {object} options
     * @param  {Store} options.store global store
     * @return {watchList}
     */
    constructor(options) {
      super(options);
      this.store = options.store;

      // events
      this.store.on('store::add', (e, data) => { this.add(data); });
      this.store.on('store::update', (e, data) => {
        const model = this.get(data.id);

        if(!model) {
          return false;
        }

        this.update(data);
      });
      this.store.on('store::remove', (e, data) => {
        const model = this.get(data.id);
        this.remove(model);
      });

      // fetch data from storage
      this.fetch();
    }


    /**
     * fetch - fetch data from local storage
     *
     * @return {array} array of stored data
     */
    fetch() {
      const items = this.store.get();
      if(!items || !items.length) {
        return false;
      }

      this.set(items);
      return items;
    }
  }

  return watchList;
});
