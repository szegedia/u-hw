'use strict';

define('videoList', ['collection', 'model'], (Collection, Model) => {

  /**
   * @class videoList
   * @extends Collection
   */
  class videoList extends Collection {

    /**
     * constructor
     *
     * @param  {object} options
     * @param  {Store} options.store global store
     * @param  {Emitter} options.emitter global emitter
     * @return {videoList}
     */
    constructor(options) {
      super(options);
      this.store = options.store;
      this.emitter = options.emitter;
      this.filterType = 'all';
      this.sortOptions = options.sortOptions || ['id', null, 'asc'];

      this.store.on('store::remove', (e, { id }) => { this.onStoreRemove(id); });
      this.emitter.on('filter::change', (e, type) => { this.setFilter(type); });
      this.emitter.on('sort::change', (e, options) => { this.setSort(options); });

      this.fetch();
    }


    /**
     * onStoreRemove - called when an item has removed from the store
     *
     * @param  {number} id model.id
     * @return {this}
     */
    onStoreRemove(id) {
      const model = this.get(id);
      model.trigger('model::unsaved');

      return this;
    }

    /**
     * getList - get collection filtered and sorted list
     *
     * @return {Model[]}
     */
    getList() {
      const list = this.get();
      const filtered = this.filterList(list);
      const sorted = this.sortList(filtered);

      return sorted;
    }

    /**
     * fetch - fetch data from server via $.ajax
     *
     * @return {AjaxObject}
     */
    fetch() {
      return $.ajax({
        dataType: 'jsonp',
        url: 'http://146.185.158.18/fake_api.php',
        success: dataSet => {
          const data = dataSet.map(data => {
            // check if the data is already stored
            if(this.store.get(data.id)) {
              return Object.assign({}, data, { saved: true });
            }
            return data;
          });
          this.set(data);
        },
        error: err => {
          console.log(err);
          this.emitter.trigger('error::fetch', err);
        }
      });
    }


    /**
     * setFilter - description
     *
     * @param  {('channel-live'|'channel-offline'|'video'|'all')} type possible values
     * @return {Model[]} model list
     */
    setFilter(type) {
      this.filterType = type;
      this.trigger('collection::filterChanged');
    }

    filterList(list) {
      let filtered;

      switch (this.filterType) {
        case 'channel-live':
          filtered = list.filter(model => {
            return model.data.type === 'channel' && !!model.data.isLive;
          });
          break;
        case 'channel-offline':
          filtered = list.filter(model => {
            return model.data.type === 'channel' && !model.data.isLive;
          });
          break;
        case 'video':
          filtered = list.filter(model => {
            return model.data.type === 'recorded';
          });
          break;
        default:
          filtered = list;
      }

      this.trigger('collection::filtered');
      return filtered;
    }


    /**
     * setSort - update sort options
     *
     * @param  {array} options [by, level, order] eg. ['country', 'location', 'desc']
     * @return {Model[]}         model list
     */
    setSort(options) {
      this.sortOptions = options;
      this.trigger('collection::sortChanged');
    }


    /**
     * sortList - sort the given array
     *
     * @param  {Model[]} list
     * @return {Model[]}      sorted list
     */
    sortList(list) {
      const [by, level, order] = this.sortOptions;

      list.sort((a, b) => {
        const itemA = level && a.data[level] || a.data;
        const itemB = level && b.data[level] || b.data;

        if(order == 'desc') {
          return itemB[by] > itemA[by] ? 1 : -1;
        }

        return itemA[by] > itemB[by] ? 1 : -1;
      });

      this.trigger('collection::sorted');
      return list;
    }
  }

  return videoList;
});
